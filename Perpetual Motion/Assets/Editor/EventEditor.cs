﻿using System.Collections;

using UnityEngine;
using UnityEditor;

namespace EventEditor {
	public class EventEditor : EditorWindow {
		public Object m_LoadedAsset;

		[MenuItem("Window/Event Editor")]
		public static void ShowWindow() {
			GetWindow<EventEditor>("Event Editor");
		}

		void OnGUI() {
			m_LoadedAsset = EditorGUILayout.ObjectField(m_LoadedAsset, typeof(TextAsset), true);
		}
	}
}
﻿using UnityEngine;

[RequireComponent(typeof(CharacterController2D))]
public class PlayerMovement : MonoBehaviour {

    [SerializeField] float jumpHeight = 4;
    [SerializeField] float timeToJumpApex = .4f;

    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;

    float moveSpeed = 6;
    float gravity = -20;

    Vector3 velocity;
    float jumpVelocity;
    float velocityXSmoothing;

    CharacterController2D controller;

    void Start() {
        controller = GetComponent<CharacterController2D>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
    }

    Vector2 input;
    float targetVelocityX;

    void Update() {
        if (controller.collisions.ABOVE || controller.collisions.BELOW) {
            velocity.y = 0;
        }

        input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (Input.GetKeyDown(KeyCode.Space) && controller.collisions.BELOW) {
            velocity.y = jumpVelocity;
        }

        targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.BELOW) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
}
﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class CharacterController2D : MonoBehaviour {

    [SerializeField] ColliderScaleMode scaleMode;
    [SerializeField] ColliderScale bounds;

    [SerializeField] LayerMask collisionMask;

    const float skinWidth = .015f;
    float maxClimbAngle = 80;
    float maxDescendAngle = 80;

    [SerializeField] int horizontalRayCount = 4;
    [SerializeField] int verticalRayCount = 4;

    float horizontalRaySpacing;
    float verticalRaySpacing;

    BoxCollider2D collider;
    RaycastOrigins raycastOrigins;

    public CollisionInfo collisions;

    Vector2 pos { get { return transform.position; }}

    void Start() {
        collider = GetComponent<BoxCollider2D>();
        CalculateRaySpacing();
    }

    public void Move(Vector2 velocity) {
        UpdateRaycastOrigins();
        collisions.RESET();
        collisions.velocityOld = velocity;

        if (velocity.y < 0) {
            DescendSlope(ref velocity);
        }
        if (velocity.x != 0) {
            HorizontalCollisions(ref velocity);
        }
        if (velocity.y != 0) {
            VerticalCollisions(ref velocity);
        }

        transform.Translate(velocity);
    }

    float directionX, directionY, rayLength, distanceToSlopeStart;
    Vector2 rayOrigin;
    RaycastHit2D hit;

    void HorizontalCollisions(ref Vector2 velocity) {
        directionX = Mathf.Sign(velocity.x);
        rayLength = Mathf.Abs(velocity.x) + skinWidth;

        for (int i = 0; i < horizontalRayCount; i++) {
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);
            hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

            if (hit) {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                if (i == 0 && slopeAngle <= maxClimbAngle) {
                    if (collisions.DESCENDINGSLOPE) {
                        collisions.DESCENDINGSLOPE = false;
                        velocity = collisions.velocityOld;
                    }

                    distanceToSlopeStart = 0;

                    if (slopeAngle != collisions.slopeAngleOld) {
                        distanceToSlopeStart = hit.distance - skinWidth;
                        velocity.x -= distanceToSlopeStart * directionX;
                    }

                    ClimbSlope(ref velocity, slopeAngle);
                    velocity.x += distanceToSlopeStart * directionX;
                }

                if (!collisions.CLIMBINGSLOPE || slopeAngle > maxClimbAngle) {
                    velocity.x = (hit.distance - skinWidth) * directionX;
                    rayLength = hit.distance;

                    if (collisions.CLIMBINGSLOPE) {
                        velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
                    }

                    collisions.LEFT = directionX == -1;
                    collisions.RIGHT = directionX == 1;
                }
            }
        }
    }

    void VerticalCollisions(ref Vector2 velocity) {
        directionY = Mathf.Sign(velocity.y);
        rayLength = Mathf.Abs(velocity.y) + skinWidth;

        for (int i = 0; i < verticalRayCount; i++) {
            rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
            hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

            if (hit) {
                velocity.y = (hit.distance - skinWidth) * directionY;
                rayLength = hit.distance;

                if (collisions.CLIMBINGSLOPE) {
                    velocity.x = velocity.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
                }

                collisions.BELOW = directionY == -1;
                collisions.ABOVE = directionY == 1;
            }
        }

        if (collisions.CLIMBINGSLOPE) {
            directionX = Mathf.Sign(velocity.x);
            rayLength = Mathf.Abs(velocity.x) + skinWidth;
            rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * velocity.y;
            hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            if (hit) {
                slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                if (slopeAngle != collisions.slopeAngle) {
                    velocity.x = (hit.distance - skinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                }
            }
        }
    }

    float moveDistance, climbVelocityY, slopeAngle, descendVelocityY;

    void ClimbSlope(ref Vector2 velocity, float slopeAngle) {
        moveDistance = Mathf.Abs(velocity.x);
        climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (velocity.y <= climbVelocityY) {
            velocity.y = climbVelocityY;
            velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
            collisions.BELOW = true;
            collisions.CLIMBINGSLOPE = true;
            collisions.slopeAngle = slopeAngle;
        }
    }

    void DescendSlope(ref Vector2 velocity) {
        directionX = Mathf.Sign(velocity.x);
        rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomRight;
        hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

        if (hit) {
            slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

            if (slopeAngle != 0 && slopeAngle <= maxDescendAngle) {
                if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x)) {
                    moveDistance = Mathf.Abs(velocity.x);
                    descendVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                    velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
                    velocity.y -= descendVelocityY;

                    collisions.slopeAngle = slopeAngle;
                    collisions.DESCENDINGSLOPE = true;
                    collisions.BELOW = true;
                }
            }
        }
    }

    Vector2 min, max;

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;

        // Draw Collider scale
        switch (scaleMode) {
            case 0:
                bounds.WIDTH = transform.localScale.x;
                bounds.HEIGHT = transform.localScale.y;
                break;
        }

        min.x = pos.x - (bounds.WIDTH / 2) + skinWidth;
        min.y = pos.y - (bounds.HEIGHT / 2) + skinWidth;
        max.x = pos.x + (bounds.WIDTH / 2) - skinWidth;
        max.y = pos.y + (bounds.HEIGHT / 2) - skinWidth;

        Gizmos.DrawLine(pos, new Vector2(pos.x, max.y));
        Gizmos.DrawLine(pos, new Vector2(pos.x, min.y));
        Gizmos.DrawLine(pos, new Vector2(min.x, pos.y));
        Gizmos.DrawLine(pos, new Vector2(max.x, pos.y));
    }

    void UpdateRaycastOrigins() {
        min.x = pos.x - (bounds.WIDTH / 2) + skinWidth;
        min.y = pos.y - (bounds.HEIGHT / 2) + skinWidth;
        max.x = pos.x + (bounds.WIDTH / 2) - skinWidth;
        max.y = pos.y + (bounds.HEIGHT / 2) - skinWidth;

        raycastOrigins.bottomLeft = new Vector2(min.x, min.y);
        raycastOrigins.bottomRight = new Vector2(max.x, min.y);
        raycastOrigins.topLeft = new Vector2(min.x, max.y);
        raycastOrigins.topRight = new Vector2(max.x, max.y);
    }

    void CalculateRaySpacing() {
        horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
        verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);

        horizontalRaySpacing = (bounds.HEIGHT - skinWidth) / (horizontalRayCount - 1);
        verticalRaySpacing = (bounds.WIDTH - skinWidth) / (verticalRayCount - 1);
    }

    struct RaycastOrigins {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }

    [System.Serializable]
    public class CollisionInfo {
        public bool ABOVE, BELOW;
        public bool LEFT, RIGHT;

        public bool CLIMBINGSLOPE;
        public bool DESCENDINGSLOPE;
        public float slopeAngle, slopeAngleOld;
        public Vector3 velocityOld;

        public void RESET() { 
            ABOVE = BELOW = LEFT = RIGHT = false;
            CLIMBINGSLOPE = DESCENDINGSLOPE = false;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }

    [System.Serializable]
    class ColliderScale {
        public float WIDTH, HEIGHT;
    }

    [System.Serializable]
    enum ColliderScaleMode : byte { Auto = 0, Manual = 1 }
}
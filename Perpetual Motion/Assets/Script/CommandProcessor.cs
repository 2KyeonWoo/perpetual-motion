﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Commands;

public class CommandProcessor {
	private byte m_CurCommand;
	private byte m_CurParameter;

	public Parameters Parameter {
		get { return (Parameters)m_CurParameter; }
		set { m_CurParameter = (byte)value; }
	}

	public Command Command {
		get { return (Command)m_CurCommand; }
		set { m_CurCommand = (byte)value; }
	}
}
